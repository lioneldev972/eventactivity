<?php
class View
{
    private $nameTemplate;
    private $donnees;
    private $layout;
    private $titre;
    
    /**
     * __construct
     *
     * @param  array $donnees
     * @param  string $titre
     *
     */
    public function __construct (array $donnees = null, string $nameTemplate = null, string $titre = null)
    {
       
        $this->donnees = $donnees;
        $this->titre = $titre;
        $this->nameTemplate = $nameTemplate;
    }


    public function templateBack()
    {	

        return  'www/templates/management/layoutGestion.phtml';
    }

    public function templateBackSimple()
    {   

      

        return  'www/templates/management/'.$this->nameTemplate.'.phtml';
    }

    public function templateFront()
    {	
       
        return 'www/templates/layout.phtml';
    }
        
    public function templateSimple()
    {   
       

        return 'www/templates/'.$this->nameTemplate.'.phtml';
    }

    public function templateB()
    {   
       
        include 'www/templates/'.$this->nameTemplate.'.phtml';
    }

    public function getViewBack(){

        
        if (isset($this->donnees)) {
            extract($this->donnees);
        }

        $title = (empty($this->titre)) ? '' : ' - '.$this->titre;
        $title = 'Event Activity '. $title;
        $template = $this->nameTemplate;

        include $this->templateBackSimple();
    }

    public function getViewBack2(){

        
        if (isset($this->donnees)) {
            extract($this->donnees);
        }

        $title = (empty($this->titre)) ? '' : ' - '.$this->titre;
        $title = 'Event Activity '. $title;
        $template = $this->nameTemplate;

        include $this->templateBack();
    }


    public function getView2()
    {      
        if (isset($this->donnees)) {
            extract($this->donnees);
        }

        $title = (empty($this->titre)) ? '' : ' - '.$this->titre;
        $title = 'Event Activity '. $title;
        $template = $this->nameTemplate;

        include $this->templateSimple();

    }

    public function getView()
    {      
        if (isset($this->donnees)) {
            extract($this->donnees);
        }
               
        $title = (empty($this->titre)) ? '' : ' - '.$this->titre;
        $title = 'Event Activity '. $title;
        $template = $this->nameTemplate;
        include $this->templateFront();

    }
}